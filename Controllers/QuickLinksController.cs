﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using NLog;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuickLinks.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickLinks.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuickLinksController : ControllerBase
    {
        private readonly IConfiguration _config;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private PhysiciansPortalContext ppEntities;
        public QuickLinksController(IConfiguration config, PhysiciansPortalContext context)
        {
            _config = config;
            ppEntities = context;
            var nlogConfig = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") {FileName = $"logs\\QuickLinksMicroservice-{DateTime.Now:MM-dd-yyyy}.log"};
            nlogConfig.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = nlogConfig;
        }
        // GET: api/<QuickLinksController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
         [Route("getlinks")]
        [HttpGet]
        public IActionResult selLinks()
        {
            var headers = Request.Headers;
            string echoID = "";

            try
            {
                var qLinks = ppEntities.PhysicianQuicklinks;

                if (headers.ContainsKey("echoid"))
                {
                    echoID = headers["echoid"].First();
                }

                if (qLinks == null)
                {
                    return Ok("");
                }
                var links = qLinks.Where(x=>x.Echoid== echoID);
                return Ok(links);
            }
            catch (Exception ex)
            {
                Logger.Info("api/qlinks/getlinks", ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// save/update links
        /// </summary>
        [Route("savelinks")]
        [HttpPost]
        public IActionResult UpdateLinks()
        {
            Logger.Info("Adding: Link");
            try
            {
                var headers = Request.Headers;
                var qLinks = new List<QuickLink>();
                if (headers.ContainsKey("links"))
                {
                    var links = headers["links"];
                    Logger.Info("header links:  ", links.ToString());
                    qLinks = JsonConvert.DeserializeObject<List<QuickLink>>(links);
                    Logger.Info("Links Size:  ", qLinks.Count);
                }
                var pLinks = ppEntities.PhysicianQuicklinks;

                //delete old links, get echoid of first element, assume qlinks is a single provider's links

                var echoId = qLinks.First().ECHOID;
                Logger.Info("Echo ID:  ", echoId);
                var linkList = ppEntities.PhysicianQuicklinks.Where(x => x.Echoid == echoId).ToList();
                // delete any that exist
                if (linkList.Count > 0)
                {
                    foreach (var link in linkList)
                    {
                        ppEntities.PhysicianQuicklinks.Remove(link);
                    }
                    ppEntities.SaveChanges();
                }

                if (qLinks.Count <= 0) return Ok();
                foreach (var q in qLinks.Where(q => (q.LinkTitle != null) && (q.LinkURL != null)))
                {
                    Logger.Info("Adding: Link Title:  " + q.LinkTitle + " URL: " + q.LinkURL);
                    var l = new PhysicianQuicklink
                    {
                        LinkTitle = q.LinkTitle, Echoid = q.ECHOID, LinkUrl = q.LinkURL
                    };
                    pLinks.Add(l);
                }

                ppEntities.SaveChanges();

                return Ok();
            }
            catch(Exception ex)
            {
                Logger.Error("Exception :: api/qlinks/savelinks", ex.Message);
                return BadRequest(ex.InnerException?.Message);
            }

        }
    }
}
