﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickLinks.Models
{
    public class QuickLink
    {
        public string ID { get; set; }
        public string ECHOID { get; set; }
        public string LinkTitle { get; set; }
        public string LinkURL { get; set; }
    }
}
