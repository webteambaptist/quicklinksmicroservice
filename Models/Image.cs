﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuickLinks.Models
{
    public partial class Image
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public string FileName { get; set; }
    }
}
