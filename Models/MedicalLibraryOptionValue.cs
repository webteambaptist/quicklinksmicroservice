﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuickLinks.Models
{
    public partial class MedicalLibraryOptionValue
    {
        public int Id { get; set; }
        public int OptionsId { get; set; }
        public string Value { get; set; }
    }
}
