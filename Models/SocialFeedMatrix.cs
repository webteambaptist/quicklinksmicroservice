﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuickLinks.Models
{
    public partial class SocialFeedMatrix
    {
        public int Id { get; set; }
        public string SubscriptionPhysicianId { get; set; }
        public string SubscribersPhysicianId { get; set; }
    }
}
